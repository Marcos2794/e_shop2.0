@extends('layouts.menuclient')
    @section('content')
    <div class="container">
    <div class="spacing-1"></div>
        <table class="table table-striped table-bordered">
            <tr>
            <th class="text-center">SKU</th>
            <th class="text-center">Producto</th>
            <th class="text-center">Cantidad</th>
            <th class="text-center">Precio Unitario</th>
            <th class="text-center">Total</th>
            <th class="text-center">Imagen</th>
            </tr>
            @foreach ($pedidos as $pedido) 
                <tr>
                <td class="text-center"> {{$pedido->sku}}</td>
                <td class="text-center"> {{$pedido->nombre}}</td>
                <td class="text-center"> {{$pedido->cantidad}}</td>
                <td class="text-center"> {{$pedido->precio_unitario}}</td>
                <td class="text-center"> {{$pedido->precio_venta}}</td>
                <td class="text-center"> <img src="/img/{{$pedido->imagen}}" class="imagen"/></td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection