@extends('layouts.menuclient')
    @section('content')
    <form method="POST" class="navbar-form" action="" >
<div class="spacing-1"></div>
    <div class="center">
    <div class="form-group">
        <input type="date" class="form-control" style="width: 150px;" name="fecha_inicio" placeholder="Marca" >
        <input type="date" class="form-control" style="width: 150px;" name="fecha_fin" placeholder="Modelo" >
        <input type="submit" class="btn btn-sm btn-info" value="Buscar">
    </div>
    </div>
</form>    
<div class="container">
    <div class="spacing-2"></div>
    <table class="table table-striped table-bordered">
        <tr>
        <th class="text-center">Numero de Pedido</th>
        <th class="text-center">Cantidad de Productos</th>
        <th class="text-center">Total de la Compra</th>
        <th class="text-center">Fecha</th>
        <th class="text-center">Detalle</th>
        </tr>

        @foreach ($pedidos as $pedido) 
                <tr>
                <td class="text-center"> {{$pedido->id_pedido}} </td>
                <td class="text-center"> {{$pedido->cantidad}} </td>
                <td class="text-center"> {{$pedido->precio_venta}} </td>
                <td class="text-center"> {{$pedido->created_at}} </td>
                <td class="text-center"><a href="pedidos/{{$pedido->id_pedido}}" class="btn btn-sm  btn-warning">Ver Orden</a></td>
                </tr>
        @endforeach
    </table>
</div>

@endsection
