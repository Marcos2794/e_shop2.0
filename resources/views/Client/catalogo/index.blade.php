@extends('layouts.menuclient')
    @section('content')
        <div class="container">   
            <div class="row"> <!-- NO MOVER DE AHI, esto alinea las imagenes -->
            @foreach ($productos as $producto)
              <!--Foreach para buscar los productos del vendedor y mostrarlos-->    
            <div class="spacing-2"></div>
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-body d-flex flex-column align-items-start">
                            <h3 class="mb-0">
                            <!--muestro la imagen de cada producto-->
                            <a href="vermas ruta">
                            <img class="card-img-top" src="/img/{{$producto->imagen}}" alt="Card image cap" width="200" height="150"></a>
                            <h2 class="display-5">{{$producto->nombre}} </h2>
                            </h3>
                            <!--cargo cada variable del producto para mostrarlo-->
                                <p class="mb-1 text-muted">Disponibles: {{$producto->stock}}</p>
                                <p class="mb-1 text-muted">Precio: {{$producto->precio}}</p>
                                {!! Form::open(['route'=>['carrito.update',$producto->sku],'method' => 'PUT']) !!}
                                {{Form::bsNumber('micantidad','1',['style'=>'width:150px;','min'=>'1'])}} 
                                <div class="spacing-2"></div>
                                
                                <a href="ruta ver mas" class="btn btn-sm  btn-warning">Ver</a><!--Boton me lleva a ver el producto-->
                                {{Form::bsSubmit('Agregar al carrito', ['class'=>'btn btn-sm btn-success'])}}
                                {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
            {{$productos->links()}}
        </div>
@endsection
          