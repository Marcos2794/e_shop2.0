@extends('layouts.menuclient')
    @section('content')
        <div class="container">    
        <a href="comprar" class="btn btn-sm  btn-info">Realizar Compra</a>
        <div class="spacing-1"></div>
            <div class="row"> <!-- NO MOVER DE AHI, esto alinea las imagenes -->
            @foreach ($carritos as $carrito)
              <!--Foreach para buscar los productos del vendedor y mostrarlos-->    
            <div class="spacing-2"></div>
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-body d-flex flex-column align-items-start">
                            <h3 class="mb-0">
                            <!--muestro la imagen de cada producto-->
                            <a href="vermas ruta">
                            <img class="card-img-top" src="/img/{{$carrito->imagen}}" alt="Card image cap" width="200" height="150"></a>
                            <h2 class="display-5">{{$carrito->nombre}} </h2>
                            </h3>
                            <!--cargo cada variable del carrito para mostrarlo-->
                                <p class="mb-1 text-muted">SKU: {{$carrito->sku}}</p>
                                {!! Form::open(['route' => ['carrito.destroy', $carrito->sku], 'method' => 'DELETE']) !!}
                                {{Form::bsNumber('cantidad',$carrito->cantidad,['style'=>'width:150px;','min'=>'1'])}} 
                                <div class="spacing-2"></div>
                                {{Form::bsSubmit('Eliminar del carrito', ['class'=>'btn btn-sm btn-danger'])}}
                                {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
            {{$carritos->links()}}
        </div>
@endsection