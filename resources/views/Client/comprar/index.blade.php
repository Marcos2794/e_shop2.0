@extends('layouts.menuclient')
    @section('content')
    <div class="container">
    {!! Form::open(['action' => 'CompraController@store','method' => 'POST','class'=>'navbar-form navbar-right']) !!}
        {{Form::bsSubmit('Comprar', ['class'=>'btn btn-sm btn-success'])}}
    {!! Form::close() !!}
        <table class="table table-striped table-bordered">
            <tr>
            <th class="text-center">SKU</th>
            <th class="text-center">Producto</th>
            <th class="text-center">Descripción</th>
            <th class="text-center">Cantidad</th>
            <th class="text-center">Precio Unitario</th>
            <th class="text-center">Total</th>
            <th class="text-center">Imagen</th>
            <th class="text-center"></th>
            </tr>
            @foreach ($carrito as $car)
                <?php $cantidad =  $car['cantidad'];?>
                    <tr>
                    @foreach($productos as $producto) 
                        @if($car->sku == $producto->sku) <!-- si el producto existe lo cargo -->
                            <td class="text-center">  {{$producto->sku}} </td>
                            <td class="text-center"> {{$producto->nombre}} </td>
                            <td class="text-center"> {{$producto->descripcion}} </td>
                        @endif 
                    @endforeach       
                    <td class="text-center"> {{$car->cantidad}} </td>
                    @foreach($productos as $producto) 
                        @if($car->sku == $producto->sku)
                        <td class="text-center"> {{$producto->precio}} </td>
                        <?php  $car->total = $producto['precio'] * $car['cantidad'];?>
                        <td class="text-center"> {{$car->total}} </td>
                        <td class="text-center"> <img src="/img/{{$producto->imagen}}" class="imagen"/></td>
                        @endif 
                    @endforeach  
                    </tr>
            @endforeach
        </table>
    </div>
@endsection