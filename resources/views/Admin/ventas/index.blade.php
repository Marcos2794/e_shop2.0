@extends('layouts.menuadmin')
  @section('content')
<div class="container">
  <h1>Mis ventas</h1>
  <form class="navbar-form navbar-left" role="search">
    <div class="form-group">
        <input type="text" class="form-control" value="<?=$nombre ?? ''?>" name="nombre" placeholder="Buscar">
    </div>
    <button type="submit" class="btn btn-default">Buscar</button>
</form>
</div>

</div>

<div class="container">
    @foreach ($productosVendidos as $producto) 
      <!--Foreach para buscar los productos del vendedor y mostrarlos-->
        <div class="spacing-2"></div>
        <div class="col-md-3" id="ventas">
            <div class="card">
                <div class="card-body d-flex flex-column align-items-start">
                    <h3 class="mb-0">
                    <!--muestro la imagen de cada producto-->
                    <img class="card-img-top" src="/img/{{$producto->imagen}}"alt="Card image cap" width="200" height="150">
                    <h2 class="display-5"> {{$producto->nombre}} </h2>
                    <h4 class="display-5">SKU: {{$producto->sku}} </h4>
                    <h4 class="display-5">Cantidad Vendida: {{$producto->cantidad}} </h4>
                    <h4 class="display-5">Total en ventas: {{$producto->precio_venta}} </h4>
                    </h3>
                </div>
            </div>
        </div>
    @endforeach
        </div>
    </div>
    
<div class="spacing-1"></div>
@endsection