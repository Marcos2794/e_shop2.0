@extends('layouts.menuadmin')
  @section('content')
    <div class="spacing-1"></div>
    <div class="container">
      <table class="table table-striped table-bordered">
        <tr>
          <th class="text-center">ID</th>
          <th class="text-center">Nombre</th>
          <th class="text-center">Apellidos</th>
          <th class="text-center">Telefono</th>
          <th class="text-center">Direccion</th>
          <th class="text-center">Email</th>
          <th class="text-center">Tipo</th>
          <th class="text-center">Estado</th>
          <th class="text-center"><a href="usuarios/create" class="btn btn-info mr-2">Nuevo</a></th>
          
        </tr>
        @if(count($users) > 0)
            @foreach($users as $usu)
                <tr>
                <td class="text-center">{{$usu->id}}</td>
                <td class="text-center">{{$usu->name}}</td>
                <td class="text-center">{{$usu->apellidos}}</td>
                <td class="text-center">{{$usu->telefono}}</td>
                <td class="text-center">{{$usu->direccion}}</td>
                <td class="text-center">{{$usu->email}}</td>
                <td class="text-center">{{$usu->role}}</td>
                <td class="text-center">{{$usu->estado}}</td>
                <td>
                <a href="usuarios/{{$usu->id}}/edit" class="btn btn-warning mr-2">Editar</a>
                {!! Form::model($users,['route'=>['usuarios.destroy',$usu->id],'method'=>'DELETE']) !!}
                <button class="btn btn-danger">Desactivar</button>  
                {!! Form::close() !!}
                </td>
                </tr>
            @endforeach
        @endif
      </table>
      <!--Divisor de pagina-->
      {{$users->links()}}
    </div>
  @endsection



<!--footer-->
