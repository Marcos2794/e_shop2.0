@extends('layouts.menuadmin')
@section('content')
<div id="navbar" class="navbar-collapse collapse">
  <img class="navbar-left" src="/img/createuser.png" style="width: 800px; height: 750px;"></a>
        <div class="row">
          <div class="col-md-4">
            <!-- Margen superior (css personalizado )-->
            <div class="spacing-1"></div>
            <fieldset>
            <legend class="center">Editar Usuario</legend>
            {!! Form::model($users,['route'=>['usuarios.update',$users->id],'method'=>'PUT','class'=>'navbar-form']) !!}
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-6">
                        <label for="name" class="col-form-label text-md-right">{{ __('Name') }}</label>
                        {{Form::bsText('name',null,['placeholder'=> 'Nueva Categoria','style'=>'width: 350px;'])}}
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                        <label for="apellidos" class="col-form-label text-md-right">{{ __('apellidos') }}</label>
                        {{Form::bsText('apellidos',null,['placeholder'=> 'Nueva Categoria','style'=>'width: 350px;'])}}
                            @if ($errors->has('apellidos'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('apellidos') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                        <label for="direccion" class="col-form-label text-md-right">{{ __('direccion') }}</label>
                        {{Form::bsText('direccion',null,['placeholder'=> 'Nueva Categoria','style'=>'width: 350px;'])}}
                            @if ($errors->has('direccion'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('direccion') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                        <label for="email" class="col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                            {{Form::bsText('email',null,['placeholder'=> 'Nueva Categoria','style'=>'width: 350px;'])}}
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                        <label for="telefono" class="col-form-label text-md-right">{{ __('Telefono') }}</label>
                            {{Form::bsText('telefono',null,['placeholder'=> 'Nueva Categoria','style'=>'width: 350px;'])}}
                            @if ($errors->has('telefono'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('telefono') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="spacing-2"></div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <select name="estado"  class="form-control" id="'estado' =>'required'" style="width: 350px;">
                                <option value="">-- Estado del Usuario --</option>
                                <option value="activo">Activo</option>
                                <option value="inactivo">Inactivo</option>
                            </select>
                        </div>
                    </div>
                    <div class="spacing-2"></div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary btn-block" style="width: 350px">
                                {{ __('Actualizar') }}
                            </button>
                        </div>   
                    </div>
            {!! Form::close() !!}
        </div>  
    </div>
  </div>
@endsection