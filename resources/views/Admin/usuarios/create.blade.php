
@extends('layouts.menuadmin')
@section('content')
<div id="navbar" class="navbar-collapse collapse">
  <img class="navbar-left" src="/img/createuser.png" style="width: 800px; height: 750px;"></a>
        <div class="row">
          <div class="col-md-4">
            <!-- Margen superior (css personalizado )-->
            <div class="spacing-1"></div>
            <fieldset>
            <legend class="center">Crear Administrador</legend>
             {!! Form::open(['action' => 'UsuarioController@store','method' => 'POST']) !!}
            
                    @csrf

                    <div class="form-group row">
                        <div class="col-md-6">
                        <label for="name" class="col-form-label text-md-right">{{ __('Name') }}</label>
                            <input id="name" type="text" style="width: 350px" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                        <label for="apellidos" class="col-form-label text-md-right">{{ __('apellidos') }}</label>
                            <input id="apellidos" type="text"  style="width: 350px" class="form-control{{ $errors->has('apellidos') ? ' is-invalid' : '' }}" name="apellidos" value="{{ old('apellidos') }}" required autofocus>

                            @if ($errors->has('apellidos'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('apellidos') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                        <label for="direccion" class="col-form-label text-md-right">{{ __('direccion') }}</label>
                            <input id="direccion" type="text"  style="width: 350px" class="form-control{{ $errors->has('direccion') ? ' is-invalid' : '' }}" name="direccion" value="{{ old('direccion') }}" required autofocus>

                            @if ($errors->has('direccion'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('direccion') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                        <label for="email" class="col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                            <input id="email" type="email" style="width: 350px" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                        <label for="telefono" class="col-form-label text-md-right">{{ __('Telefono') }}</label>
                            <input id="telefono" type="telefono" style="width: 350px" class="form-control{{ $errors->has('telefono') ? ' is-invalid' : '' }}" name="telefono" value="{{ old('telefono') }}" required>

                            @if ($errors->has('telefono'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('telefono') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                        <label for="password" class="col-form-label text-md-right">{{ __('Password') }}</label>
                            <input id="password" type="password" style="width: 350px" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                        <label for="password-confirm" class="col-form-label text-md-right">{{ __('Confirm Password') }}</label>
                            <input id="password-confirm" type="password" style="width: 350px" class="form-control" name="password_confirmation" required>
                            <div class="spacing-2"></div>
                            <button type="submit" class="btn btn-primary btn-block" style="width: 350px">
                               login
                            </button>
                        </div>   
                    </div>
                 {!! Form::close() !!}
        </div>  
    </div>
  </div>
@endsection

