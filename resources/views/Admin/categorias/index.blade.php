@extends('layouts.menuadmin')
  @section('content')
  <div class="categoria">
    <div class="col-md-4 ">
      <div class="spacing-1"></div>
      {!! Form::open(['action' => 'CategoriaController@store','method' => 'POST','class'=>'navbar-form']) !!}
        <fieldset>
        <legend class="center">Categoria</legend>
        <!-- Div espaciador -->
        <div class="spacing-2"></div>
          {{Form::bsText('nombre',null,['placeholder'=> 'Nueva Categoria','style'=>'width: 350px;'])}}
          <div class="spacing-2"></div>
          <div class="form-group">
            <select name="padre_id"  class="form-control" id="'padre_id' =>'required'" style="width: 350px;">
            <option value="N/A">-- Escoja categoria padre --</option>
              @foreach($cate as $padre)
              <option value="{{$padre['id']}}">{{$padre['nombre']}}</option>
              @endforeach
            </select>
          </div>
        <!-- boton #editar para activar la funcion click y enviar el los datos-->
        <div class="row">
          <div class="col-xs-6 col-xs-offset-2">
            <div class="spacing-2"></div>
            {{Form::bsSubmit('Crear', ['class'=>'btn btn-success btn-block'])}}
          </div>
        </div>
      </fieldset>
    {!! Form::close() !!}
  </div>
</div> 
    <div class="container">
      <table class="table table-striped table-bordered">
        <tr>
          <th class="text-center">ID</th>
          <th class="text-center">Nombre</th>
          <th class="text-center">Categoria Padre</th>
          <th class="text-center">Opciones</th>
        </tr>
        @foreach($categorias as $hija)
            <tr>
            <td class="text-center">{{$hija->id}}</td>
            <td class="text-center">{{$hija->nombre}}</td>
            @if($hija->padre_id != null)
              @foreach($cate as $padre)
                @if($hija->padre_id == $padre->id)
                <td class="text-center">{{$padre->nombre}}</td>
                @endif
              @endforeach  
            @else
            <td class="text-center">N/A</td>
            @endif
            <td td class="text-center" style="width: 200px">
            <a href="categorias/{{$hija->id}}/edit" class="btn btn-warning"> 
            <span class="glyphicon glyphicon-wrench"></span> Editar</a>
            {!! Form::open(['route' => ['categorias.destroy', $hija->id], 'method' => 'DELETE']) !!}
              <button class="btn btn-danger">Eliminar</button>                           
            {!! Form::close() !!}
            </td>
            </tr>
        @endforeach
      </table>
      <!--Divisor de pagina-->
    {{$categorias->links() }}
    </div>
  @endsection

 