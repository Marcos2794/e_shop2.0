@extends('layouts.menuadmin')
  @section('content')
<div class="center">
  <!-- Margen superior (css personalizado )-->
  <div class="spacing-1"></div>

    {!! Form::model($categorias,['route'=>['categorias.update',$categorias->id],'method'=>'PUT','class'=>'navbar-form']) !!}
    <!-- Estructura del formulario -->
        <fieldset>
        <legend class="center">Actualizar Categoria</legend>
        <div class="spacing-2"></div>
        {{Form::bsText('nombre',null,['placeholder'=> 'Nueva Categoria','style'=>'width: 350px;'])}}
        <!-- Div espaciador -->
        <div class="spacing-2"></div>
        <div class="form-group">
          <select name="padre_id"  class="form-control" id="'padre_id' =>'required'" style="width: 350px;">
            <option value="N/A">-- Escoja categoria padre --</option>
              @foreach($cate as $padre)
              <option value="{{$padre['id']}}">{{$padre['nombre']}}</option>
              @endforeach
          </select>
        </div>
        <!-- boton #editar para activar la funcion click y enviar el los datos-->
        <div class="row">
            <div class="center">
            <div class="spacing-2"></div>
            {{Form::bsSubmit('Actualizar', ['class'=>'btn btn-success'])}}
            </div>
        </div>
        </fieldset>  
    {!! Form::close() !!}
  <img class="navbar-form navbar-rigth" src="/img/update.png" width="500" height="450"></a>
  </div>
</div>
<div class="spacing-1"></div>
@endsection