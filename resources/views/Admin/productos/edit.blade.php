@extends('layouts.menuadmin')
  @section('content')
    <div id="navbar" class="navbar-collapse collapse"> 
    <div class="spacing-1"></div>
    <img class="navbar-left" src="/img/{{$productos->imagen}}" style="width: 600px; height: 600px;"></a>
      <div class="row">
        <div class="col-md-4">
            <!-- Margen superior (css personalizado )-->
            <div class="spacing-1"></div>
            {!! Form::model($productos,['route'=>['productos.update',$productos->sku],'method'=>'PUT','class'=>'navbar-form']) !!}
              <!-- Estructura del formulario -->
              <fieldset>

                <legend class="center">Editar Producto</legend>
              
                <!-- Div espaciador -->
                <div class="spacing-2"></div>
                <!-- Caja de texto para nombre -->
                {{Form::bsText('nombre',null,['placeholder'=> 'Nombre'])}}
                
                <!-- Caja de texto para sku -->
                {{Form::bsText('sku',null,['placeholder'=> 'SKU'])}}

                <div class="spacing-2"></div>
                <!--  Caja de texto para stock -->
                {{Form::bsNumber('stock',null,['placeholder'=> 'Cantidad en Stock'])}}

                <!-- Caja de texto para precio -->
                {{Form::bsNumber('precio',null,['placeholder'=> 'Precio'])}}

                <!-- Div espaciador -->
                <div class="spacing-2"></div>
                <!-- Caja de texto para descripcion -->
                {{Form::bsText('descripcion',null,['placeholder'=> 'Descripcion','style'=>'width: 350px;'])}}
            
                <div class="spacing-2"></div>
                <!--  categoria-->
                <div class="form-group">
                <select name="id_categoria"  class="form-control" id="id_categoria" style="width: 350px;">
                <option value="">-- Escoja categoria --</option>
                  @foreach($categorias as $categoria)
                  <option value="{{$categoria['id']}}">{{$categoria['nombre']}}</option>
                  @endforeach
                </select>
                </div>

                <!-- imagen-->
                <div class="spacing-2"></div>
                <div class="form-group">
                  <input type="file"  name="imagen" accept="image/*" required>
                </div>

              <!-- Div espaciador -->
              <div class="spacing-2"></div>
                <!-- boton #crear para activar la funcion click y enviar el los datos-->
                <div class="row">
                  <div class="col-xs-6 col-xs-offset-2">
                    <div class="spacing-2"></div>
                    {{Form::bsSubmit('Crear', ['class'=>'btn btn-success btn-block'])}}
                  </div>
                </div>
              </fieldset>
          {!! Form::close() !!}
        </div>
      </div>  
    </div> 
@endsection      