@extends('layouts.menuadmin')
  @section('content')
  <div class="spacing-1"></div>
    <div class="categoria">  
      <div class="container">
        <table class="table table-striped table-bordered">
          <tr>
            <th class="text-center">SKU</th>
            <th class="text-center">Producto</th>
            <th class="text-center">Cantidad</th>
            <th class="text-center">Precio</th>
            <th class="text-center">Descripción</th>
            <th class="text-center">Categoria</th>
            <th class="text-center">Imagen</th>
            <th class="text-center"><a href="productos/create" class="btn btn-info mr-2">Nuevo</th>
          </tr>
          <!--Cargo los atributos de productos con el foreach-->
              @foreach($productos as $producto)
              <tr>
              <td class="text-center">{{$producto->sku}} </td>
              <td class="text-center">{{$producto->nombre}}</td>
              <td class="text-center">{{$producto->stock}}</td>
              <td class="text-center">{{$producto->precio}}</td>
              <td class="text-center">{{$producto->descripcion}}</td>
              <!--Busco el nombre de la categoria con el id del producto-->
              @foreach($categorias as $categoria)
                @if($categoria->id == $producto->id_categoria)
                <td class="text-center">{{$categoria->nombre}}</td>
                @endif
              @endforeach
              <td class="text-center"> <img src="/img/{{$producto->imagen}}" class="imagen"/></td>
              <td class="text-center">
              <a href="productos/{{$producto->sku}}/edit"class="btn btn-warning">Editar</a>
              {!! Form::open(['route' => ['productos.destroy', $producto->sku], 'method' => 'DELETE']) !!}
              <button class="btn btn-danger">Eliminar</button>                           
              {!! Form::close() !!}
              </td>
              </tr>
              @endforeach
        </table>
        <!--Divisor de pagina-->
        {{$productos->links() }}
      </div>
    </div>
  </div>
@endsection

