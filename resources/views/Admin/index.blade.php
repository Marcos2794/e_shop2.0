
@extends('layouts.menuadmin')
@section('content')
<div class="container">
    <div class="spacing-2"></div>

      <div class="p-3 mb-2" style="background:#559E54">
        <div class="container">
            <h1 class="center" style="color:#fffefe">E-SHOP ADMIN</h1>
            @foreach ($registrados as $usu) 
                <h3 style="color:#fffefe" class="text-white">
                Cantidad de clientes: {{$usu->id}}
                </h3>
            @endforeach
            @foreach ($adminestadisticas as $estadisticas) 
                <h3 style="color:#fffefe" class="text-white">
                Cantidad de productos vendidos: {{$estadisticas->cantidad}}
                </h3>
                <h3 style="color:#fffefe" class="text-white">
                Monto total de ventas: {{$estadisticas->precio_venta}}
                </h3>
            @endforeach 
        </div> 
      </div> 
        <?php
          //'../Slider/slider.php';
        ?>
  </div> 
</div>
@endsection

