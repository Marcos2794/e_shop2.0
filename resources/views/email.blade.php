<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, productos-scalable=no, initial-scale=1.0">
    <title>Llamado de emergencia</title>
</head>
<body>
    <p>Estos son los productos que se encuentran con bajo stock</p>
    
	@if(count($productos) > 0)
            @foreach($productos as $producto)
			<ul>
				<li>SKU: {{ $producto->sku }}</li>
				<li>Nombre: {{ $producto->nombre }}</li>
				<li>Stock: {{ $producto->stock }}</li>
			</ul>
            @endforeach
    @endif
        
</body>
</html>