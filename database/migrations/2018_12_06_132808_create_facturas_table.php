<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturas', function (Blueprint $table) {
            $table->integer('id_pedido')->unsigned();
            $table->string('sku')->index();
            $table->string('nombre');
            $table->integer('id_usuario')->unsigned();
            $table->integer('cantidad');
            $table->integer('precio_unitario');
            $table->integer('precio_venta');
            $table->string('imagen');
            $table->timestamps();
            $table->foreign('id_usuario')
            ->references('id')->on('users');
            $table->foreign('sku')
            ->references('sku')->on('productos');
            $table->foreign('id_pedido')
            ->references('id')->on('pedidos');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturas');
    }
}
