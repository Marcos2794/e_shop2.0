<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->string('sku');
            $table->string('nombre');
            $table->integer('stock');
            $table->integer('precio');
            $table->string('descripcion');
            $table->integer('id_categoria')->unsigned();
            $table->string('imagen');
            $table->timestamps();
            $table->primary('sku');
            $table->foreign('id_categoria')
            ->references('id')->on('categorias')
            ->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
