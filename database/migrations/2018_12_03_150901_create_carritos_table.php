<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarritosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carritos', function (Blueprint $table) {
            $table->string('sku')->index();
            $table->string('nombre');
            $table->integer('id_usuario')->unsigned();
            $table->integer('cantidad');
            $table->string('imagen');
            $table->timestamps();
            $table->foreign('id_usuario')
            ->references('id')->on('users');
            $table->foreign('sku')
            ->references('sku')->on('productos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carritos');
    }
}
