<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
    protected $primaryKey = 'id_pedido';
    protected $fillable = [
        'id_pedido', 'sku', 'nombre', 'id_usuario', 'cantidad', 'precio_unitario', 'precio_venta', 'imagen', 'created_at', 'updated_at'
    ];
}
