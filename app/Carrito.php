<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carrito extends Model
{
    protected $primaryKey = 'sku';
    protected $fillable = [
        'sku', 'nombre','id_usuario', 'cantidad', 'imagen'
    ];

    public function scopeCantidad($query, $cantidad){

        if($cantidad!=""){
            $query->where('stock','>', $cantidad);
        }
    }
}

