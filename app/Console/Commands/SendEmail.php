<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Producto;
use Log;
use Mail;
use App\Mail\ProductosEmail;

class SendEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'productos:email {stock}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send the low stock';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public $stock;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $receiver = 'alonsorp329@gmail.com';
        $stock = $this->argument('stock');
        
        $productos = Producto::where('stock', '<=', $stock)->get();

        Mail::to($receiver)->send(new ProductosEmail($productos));

        Log::info("Email enviado exitosamente");
    }
}
