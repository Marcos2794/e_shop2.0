<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $primaryKey = 'sku';
    protected $fillable = [
        'sku', 'nombre', 'stock', 'precio', 'descripcion', 'id_categoria', 'imagen'
    ];

}
