<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsuarioPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'apellidos' => 'required',
            'telefono' => 'required',
            'email' => 'required|email|unique:users,email',
            'direccion' => 'required',
            'password' => 'required',
            'password_confirmation' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'name.required' =>'El campo nombre es requerido',
            'apellidos.required' =>'El campo apellidos es requerido',
            'telefono.required' =>'El campo telefono es requerido',
            'email.required' =>'El campo email es requerido',
            'email.unique' =>'El email ya esta asociado a otro usuario',
            'direccion.required' =>'El campo direccion es requerido',
            'password.required' =>'El campo contraseña es requerido',
            'password_confirmation.required' =>'El campo verificar contraseña es requerido'
        ];
    }
}
