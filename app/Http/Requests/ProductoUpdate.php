<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductoUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            
            'sku' => 'required',
            'nombre' => 'required',
            'stock' => 'required',
            'precio' => 'required',
            'descripcion' => 'required',
            'id_categoria' => 'required',
            'imagen' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'sku.required' => 'SKU del producto requerido',
            'nombre.required' => 'Nombre del producto requerido',
            'stock.required' => 'Cantidad en stock del producto requerido',
            'precio.required' => 'Precio del producto requerido',
            'descripcion.required' => 'Descripcion del producto requerido',
            'id_categoria.required' => 'Categoria del producto requerido',
            'imagen.required' => 'Imagen del producto requerido'
        ];
    }
}
