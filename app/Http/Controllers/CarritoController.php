<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;
use App\Carrito;
use App\Http\Requests\CatalogoPost;
use Auth;
use App\User;


class CarritoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userid = Auth::id();
        return view('client.carrito.index', ['carritos'=> Carrito::where('id_usuario','=', $userid)->paginate(5)]);
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(CatalogoPost $request, $sku)
    {
       
        $userid = Auth::id();
        $productos = Producto::find($sku);
        if($productos['stock'] < $request['micantidad']){
            return back()->withErrors(['micantidad' => 'La cantidad de stock es menor a la deseada']);
        }
        $validacarrito = Carrito::where('sku', '=', $sku)->where('id_usuario','=', $userid)->get();
        if($validacarrito == '[]'){
            Carrito::create([
                'sku'=>$productos['sku'],
                'nombre' => $productos['nombre'],
                'id_usuario' => $userid,
                'cantidad'=> $request['micantidad'],
                'imagen' => $productos['imagen']
                ]);
            return back(); 
        }
        foreach($validacarrito as $carro)
        {
            $cant = $carro->cantidad;
        }

        $cantidad = $cant + $request['micantidad'];
        if($cantidad > $productos['stock']){
            return back()->withErrors(['micantidad' => 'La cantidad en el carrito mas la nueva cantiada es mayor a la de stock']);
        }
        $validacarrito->cantidad = $cantidad;
        Carrito::where('sku', '=', $sku)->where('id_usuario','=', $userid)->update(['cantidad' => $cantidad]);
        return back();
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function destroy($sku)
    {
        $carrito = Carrito::find($sku)->delete();
        return back()->with('info', 'Eliminado correctamente');
    }
}
