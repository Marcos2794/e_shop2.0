<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\User;
use App\Http\Requests\UsuarioPost;
use Illuminate\Support\Facades\Hash;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use RegistersUsers;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function index()
    {
        return view('admin.usuarios.index', ['users' => User::paginate(8)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.usuarios.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsuarioPost $request)
    {
        $request['estado'] = 'activo';
        $request['role'] = 'admin';
        if ($request['password'] == $request['password_confirmation']) {
            User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'telefono' => $request['telefono'],
            'password' => Hash::make($request['password']),
            'apellidos' => $request['apellidos'],
            'direccion' => $request['direccion'],
            'estado' => $request['estado'],
            'role' => $request['role'],
            ]);
            return redirect('admin/usuarios')->with('info', 'Producto actualizado con éxito');

        }
        return back()->withErrors(['password' => 'Las contraseñas no son iguales']);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.usuarios.edit', ['users' => User::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $usuario = User::find($id);

        $usuario->fill($request->except('role','password'))->save();
        return redirect('admin/usuarios')->with('info', 'Producto actualizado con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        User::where('id', '=', $id)->update(['estado' => 'inactivo']);
        return back()->with('info', 'Desactivado correctamente');
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function desactivar($id)
    {;
       
    }

}
