<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //si esta logeado lo redirecciona segun su rol
        if (Auth::check() && Auth::user()->role=='admin') {
            return redirect('admin');
        }
        return redirect('client');
    }
}
