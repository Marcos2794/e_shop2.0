<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Factura;
use Auth;
use App\User;
use DB;


class PedidoController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userid = Auth::id();
        //return view('client.pedidos.index',['pedidos' =>Factura::where('id_usuario','=', $userid)->paginate(5)]);
        return view('client.pedidos.index',
        ['pedidos' => Factura::select(
            DB::raw("id_pedido, SUM(cantidad) as cantidad, SUM(precio_venta) as precio_venta, created_at"))
            ->where('id_usuario','=', $userid)
            ->groupBy("id_pedido")
            ->orderBy('id_pedido', 'DESC')
            ->paginate(5)]);
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductoPost $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_pedido)
    {
        $userid = Auth::id();
        return view('client.pedidos.show', ['pedidos'=> Factura::where('id_usuario','=', $userid)->where('id_pedido','=', $id_pedido)->paginate(5)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($sku)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductoUpdate $request, $sku)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($sku)
    {
    }
}
