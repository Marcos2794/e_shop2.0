<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Producto;
use App\Http\Requests\ProductoPost;
use App\Http\Requests\ProductoUpdate;
use App\Categoria;
use Illuminate\Support\Facades\Storage;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
       $this->middleware('auth');
       $this->middleware('admin');
    }
    public function index()
    {
        return view('admin.productos.index',['productos' =>Producto::paginate(5),'categorias' => Categoria::all()]);
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.productos.create', ['categorias' => Categoria::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductoPost $request)
    {
        //IMAGE 
        
        $producto = New Producto();
        //si el request me retorna una imagen(archivo file), lo guarda en la ruta img
        if($request->hasFile('imagen')){
            $file = $request->file('imagen');
            $name = time().$file->getClientOriginalName();
            $producto->imagen = $name;
            $file ->move(public_path().'/img/',$name);
        }
        //guardo todos los valores del request excepto la imagen que la cargo asi = $producto->imagen = $name;
        $producto->fill($request->except('imagen'))->save();
        return redirect('admin/productos')->with('info', 'Producto creado con éxito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($sku)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($sku)
    {
        return view('admin.productos.edit', ['productos' => Producto::find($sku),'categorias' => Categoria::all()]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductoUpdate $request, $sku)
    {
        
        $producto = Producto::find($sku);
        if($request->hasFile('imagen')){
            $file = $request->file('imagen');
            $name = time().$file->getClientOriginalName();
            $producto->imagen = $name;
            $file ->move(public_path().'/img/',$name);
        }
        $producto->fill($request->except('imagen'))->save();
        return redirect('admin/productos')->with('info', 'Producto actualizado con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($sku)
    {
        $producto = Producto::find($sku)->delete();
        return back()->with('info', 'Eliminado correctamente');
    }

    public function cronjob($stock)
    {
        $productos = Producto::where('stock', '<=', $stock);
        return $productos;
    }
}
