<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categoria;
use App\CategoriaPadre;
use App\Http\Requests\CategoryPost;
use Session;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    public function index()
    {

        return view('admin.categorias.index', ['categorias' => Categoria::paginate(5),'cate'=> Categoria::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryPost $request)
    {
        if($request['padre_id']== 'N/A'){
            $request['padre_id'] = null;
        }
        //crear el categoria
        Categoria::create($request->all());
        Session::flash('message','Se creo la categoria correctamente');
        return back(); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.categorias.edit', ['categorias' => Categoria::find($id),'cate'=> Categoria::all()]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request['padre_id']== 'N/A'){
            $request['padre_id'] = null;
        }
        $categorias = Categoria::find($id);
        $categorias->fill($request->all())->save();

        return redirect('admin/categorias')->with('info', 'Categoria actualizada con éxito');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /*
        $categorias = Categoria::find($id)->delete();
        return back()->with('info', 'Eliminado correctamente');
        */

        try{
            $categorias = Categoria::find($id)->delete();
            return back()->with('info', 'Eliminado correctamente');
        }catch(\Illuminate\Database\QueryException $e){
            return back()->withErrors(['info' => 'No se puede eliminar esta categoria']);
        }

    }
}
