<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;
use App\Factura;
use App\Pedido;
use App\Carrito;
use Auth;
use App\User;

class CompraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userid = Auth::id();
        return view('Client.comprar.index', ['productos' => Producto::All(),'carrito'=> Carrito::where('id_usuario','=', $userid)->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userid = Auth::id();
        $carrito = Carrito::where('id_usuario','=', $userid)->get();

  //inserto el pedido
    $pedido = new Pedido();
    
    $pedidos = Pedido::create(['id_usuario'=> $userid]); //obtengo el ultimo pedido del usuario
      
    /*if ($pedidos) {// busco el id del pedido que sera el numero de factura y la fecha
        foreach ($pedidos as $pedido) {
            $id_pedido = $pedido['id_pedido'];
        }
    };*/

    if ($carrito != '[]') {
        foreach ($carrito as $car) { // busco en el carrito
            $cantidad =  $car['cantidad'];
            $sku =  $car['sku'];
            $nombre =  $car['nombre'];
            $imagen =  $car['imagen'];
            $productos = Producto::where('sku', '=', $sku)->where('stock','>=', $cantidad)->get();
            if ($productos !='[]') {// si el producto existe lo cargo a la factura
                foreach ($productos as $producto) {
                    $precio_unitario = $producto->precio;
                    $precio_venta = $precio_unitario * $cantidad; // saco el precio de venta total
                    $factura = new Factura();
                    $factura->id_pedido = $pedidos->id;
                    $factura->sku = $producto['sku'];
                    $factura->nombre = $producto['nombre'];
                    $factura->id_usuario =$userid;
                    $factura->cantidad = $cantidad;
                    $factura->precio_unitario = $precio_unitario;
                    $factura->precio_venta = $precio_venta;
                    $factura->imagen = $producto['imagen'];
                    $factura->save();  
                }
            } else { // si un solo producto no existe cancelo todas las operaciones
                Carrito::where('sku', '=', $sku)->where('id_usuario','=', $userid)->delete();
                Pedido::find($pedidos->id)->delete();

                $facturas = Factura::where('id_pedido', '=', $pedidos->id)->get();
                if($facturas !='[]'){
                    Factura::find($pedidos->id)->delete();
                }
                return back()->withErrors(['El producto que desea adquirir esta agotado, por lo tanto lo eliminamos de su carrito intente su compra nuevamente']);
            };
        }

        $facturas = Factura::where('id_pedido', '=', $pedidos->id)->get();
        if ($facturas !='[]') {// busco el producto ya facturado
            foreach ($facturas as $factura) {
                $sku =$factura->sku;
                $productos = Producto::where('sku', '=', $sku)->get();
                if ($productos !='[]') {// busco la cantidad en stock y la rebajo de la compra
                    foreach ($productos as $producto) {
                            $stock = $producto->stock;
                            $stock = $stock - $factura->cantidad;
                            Producto::where('sku', '=', $sku)->update(['stock' => $stock]);
                            
                    }
                }
            }
        }

    } else {
        Pedido::find($pedidos->id)->delete();
        return back()->withErrors(['No tiene productos en el carrito']);

    };
    Carrito::where('id_usuario','=', $userid)->delete();
  

    return redirect('client/pedidos')->withErrors(['Compra Exitosa']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
