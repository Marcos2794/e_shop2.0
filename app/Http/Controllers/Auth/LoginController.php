<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;
use Redirect;

class LoginController extends Controller
{

    use AuthenticatesUsers;

    public function login()
    {
        $credentials = $this->validate(request(), [
            'email' => 'required|email|string',
            'password'  => 'required|string'

        ], [
            'email.required' =>'El campo email es requerido',
            'password.required' =>'El campo contraseña es requerido',

        ]);

        if (Auth::attempt($credentials, true)) {
            return $this->redirectPath();
        }
        return back()->withErrors(['email' => 'Estas credenciales no coinciden con nuestros registros']);
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        return redirect('/');
    }

    public function redirectPath()
    {
        if (Auth::check() && Auth::user()->role =='admin') {
            return redirect('admin');
        }
        return redirect('client');
    }
}
