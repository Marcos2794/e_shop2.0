<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Form;

class FormServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Form::component('bsText', 'components.userform.text', ['texto', 'value'=>null, 'attributes'=>[]]);
        Form::component('bsNumber', 'components.userform.number', ['numero', 'value'=>null, 'attributes'=>[]]);
        Form::component('bsPassword', 'components.userform.password', ['password', 'value'=>null, 'attributes'=>[]]);

        //boton
        Form::component('bsSubmit', 'components.userform.submit', [ 'value'=>'Registrar', 'attributes'=>[]]);
        
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
