  
CREATE DATABASE eshop;

USE eshop;

CREATE TABLE usuarios(
	`id` serial PRIMARY KEY NOT NULL,
    `nombre` VARCHAR(45) NOT NULL,
    `apellidos` VARCHAR(45) NOT NULL,
    `telefono` VARCHAR(45) NOT NULL,
    `direccion` VARCHAR(45) NOT NULL,
    `email` VARCHAR(45) NOT NULL,
	`tipo` VARCHAR(6) NOT NULL,
    `pass` VARCHAR(45) NOT NULL,
	`estado` text NOT NULL
);

CREATE TABLE `eshop`.`categoriapadre` (
  `id_categoriapadre` serial PRIMARY KEY NOT NULL,
  `nombre` VARCHAR(45) NOT NULL);
  
  
CREATE TABLE `eshop`.`categoria` (
  `id_categoria` serial PRIMARY KEY NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `id_categoriapadre` BIGINT UNSIGNED NOT NULL,
  FOREIGN KEY (`id_categoriapadre`) REFERENCES `eshop`.`categoriapadre` (`id_categoriapadre`));
  
  
  
CREATE TABLE `eshop`.`producto` (
  `sku` VARCHAR(45) PRIMARY KEY NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `stock` INT NOT NULL,
  `precio` FLOAT NOT NULL,
  `descripcion` VARCHAR(45) NOT NULL,
  `id_categoria` BIGINT UNSIGNED NOT NULL,
  `imagen` text NOT NULL,
  FOREIGN KEY (`id_categoria`) REFERENCES `eshop`.`categoria` (`id_categoria`));
  
  

CREATE TABLE `eshop`.`carrito` (
  `sku` VARCHAR(45) NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `id_usuario` BIGINT UNSIGNED NOT NULL,
  `cantidad` INT NOT NULL,
  `imagen` text NOT NULL,
  FOREIGN KEY (`sku`) REFERENCES `eshop`.`producto` (`sku`),
  FOREIGN KEY (`id_usuario`) REFERENCES `eshop`.`usuarios` (`id`));
 
 
 CREATE TABLE `eshop`.`pedido` (
  `id_pedido` serial PRIMARY KEY NOT NULL,
  `id_usuario` BIGINT UNSIGNED NOT NULL,
  `fecha` DATE NOT NULL,
  FOREIGN KEY (`id_usuario`) REFERENCES `eshop`.`usuarios` (`id`));
 
 
 CREATE TABLE `eshop`.`factura` (
  `id_pedido` BIGINT UNSIGNED NOT NULL,
  `sku` VARCHAR(45) NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `id_usuario` BIGINT UNSIGNED NOT NULL,
  `cantidad` INT NOT NULL,
  `precio_unitario` FLOAT NOT NULL,
  `precio_venta` FLOAT NOT NULL,
  `fecha` DATETIME NOT NULL,
  `imagen` text NOT NULL,
  
  FOREIGN KEY (`id_usuario`) REFERENCES `eshop`.`usuarios` (`id`),
  FOREIGN KEY (`id_pedido`) REFERENCES `eshop`.`pedido` (`id_pedido`));
  
/*
Insert usuarios
*/  






INSERT INTO `categoria_padres` (`nombre`,`created_at`,`updated_at`) VALUES ('Tecnologia','2018-11-28 12:39:38', '2018-11-28 12:39:38');
/*
Insert categoria
*/ 

INSERT INTO `categoria` (`id_categoria`,`nombre`) VALUES (1,'Informatica');
INSERT INTO `categoria` (`id_categoria`,`nombre`) VALUES (2,'Vehiculos');
INSERT INTO `categoria` (`id_categoria`,`nombre`) VALUES (3,'Calzado');
INSERT INTO `categoria` (`id_categoria`,`nombre`) VALUES (5,'Fotografia');


/*
Insert productos
*/  
INSERT INTO `producto` (`sku`,`nombre`,`stock`,`precio`,`descripcion`,`id_categoria`,`imagen`) VALUES ('1w50oc','Magista Obra',5,40000,'Tacos magista obra',3,'142919.jpg');
INSERT INTO `producto` (`sku`,`nombre`,`stock`,`precio`,`descripcion`,`id_categoria`,`imagen`) VALUES ('2dcmgj','Tripode',14,35000,'Tripode semi profesional Nikon, en aluminio.',5,'tripode-benro-t560-cabezal-p-fotografia-c-estuche-D_NQ_NP_780466-MLA26705521267_012018-F.jpg');
INSERT INTO `producto` (`sku`,`nombre`,`stock`,`precio`,`descripcion`,`id_categoria`,`imagen`) VALUES ('3076i3','Nikon D7000',13,750000,'Nikon D7000, lente macro, memoria de 32gb.',5,'d7100blk.jpg');
INSERT INTO `producto` (`sku`,`nombre`,`stock`,`precio`,`descripcion`,`id_categoria`,`imagen`) VALUES ('ayn3kc','Dell Inspiron',0,1000000,'Laptop Dell Inspiron Gaming 4K',1,'Dell-Inspiron-15-7000_1.jpg');
INSERT INTO `producto` (`sku`,`nombre`,`stock`,`precio`,`descripcion`,`id_categoria`,`imagen`) VALUES ('f63ywb','Mouse',1,50000,'Razer mamba chroma',1,'Razer-Mamba-Tournament-Edition-16000DPI-Gaming-Mouse-5G-Laser-Sensor-Chroma-Light-Ergonomic-Computer-Game-Mouse.jpg_640x640.jpg');
INSERT INTO `producto` (`sku`,`nombre`,`stock`,`precio`,`descripcion`,`id_categoria`,`imagen`) VALUES ('hwufib','Memoria USB',10,15000,'Memoria usb kingston de 16g plateada, alumini',1,'cp-kingston-dtse9h16gb_1.jpg');
INSERT INTO `producto` (`sku`,`nombre`,`stock`,`precio`,`descripcion`,`id_categoria`,`imagen`) VALUES ('k16vq3','Botas Timberland',2,80000,'Botas timberland para hombre',3,'botas-de-senderismo-hombre-timberland-euro-hiker-jacquard-boot-blanco.jpg');
INSERT INTO `producto` (`sku`,`nombre`,`stock`,`precio`,`descripcion`,`id_categoria`,`imagen`) VALUES ('m632d3','Razer Blade',4,1200000,'Razer Blade 14&#34; 4k',1,'razer-blade-15-gallery08-gaming-laptop.jpg');
INSERT INTO `producto` (`sku`,`nombre`,`stock`,`precio`,`descripcion`,`id_categoria`,`imagen`) VALUES ('p2mhks','Retrovisor',4,50000,'Espejo retrovisor inteligente',2,'SKU254862_16.jpg');

/*
Insert pedido
*/ 
INSERT INTO `pedido` (`id_pedido`,`id_usuario`,`fecha`) VALUES (2,1,'2018-11-19');


/*
Insert factura
*/ 

INSERT INTO `factura` (`id_pedido`,`sku`,`nombre`,`id_usuario`,`cantidad`,`precio_unitario`,`precio_venta`,`fecha`,`imagen`) VALUES (2,'7voi7v','Adidas NEO',1,1,40000,40000,'2018-11-19 01:53:56','Original-New-Arrival-2018-Adidas-NEO-Label-Men-s-Skateboarding-Shoes-Sneakers.jpg_640x640.jpg');
INSERT INTO `factura` (`id_pedido`,`sku`,`nombre`,`id_usuario`,`cantidad`,`precio_unitario`,`precio_venta`,`fecha`,`imagen`) VALUES (2,'m632d3','Razer Blade',1,1,1200000,1200000,'2018-11-19 01:53:56','razer-blade-15-gallery08-gaming-laptop.jpg');


SELECT * FROM eshop.users;
SELECT * FROM eshop.categorias;
SELECT * FROM eshop.productos;
SELECT * FROM eshop.carritos;

SELECT * FROM eshop.facturas;
SELECT * FROM eshop.pedidos;

drop database eshop;
create database eshop;



DELETE FROM carritos where sku != '1';
use eshop;
DELETE FROM facturas where id_pedido != '0';
DELETE FROM pedidos where id != '0';

USE eshop;
INSERT INTO `users` (`name`,`email`,`telefono`,`password`,`apellidos`,`direccion`,`estado`,`role`,`remember_token`,`created_at`,`updated_at`) VALUES ( 'Marcos', 'marcosab2794@gmail.com', '84452340', '$2y$10$pzynYqSp6mm8zwtbJEdSTuB50bqu336ajo7vCNlY96Q6ErScboZTi', 'Avila', 'En la U', 'activo', 'admin', 'asjwdku7CCexGhv9WxhvJk95RrR30ElqYYNOUGC4ys1M9UX21AlPei0mVIm1', '2018-12-08 17:51:28', '2018-12-08 17:51:28');
