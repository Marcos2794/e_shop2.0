<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//ruta de inicio
Route::get('/', 'HomeController@index')->name('home');

//ruta home del administrador
Route::get('/admin', 'AdminController@index')->name('admin');

//admin/productos
Route::prefix('admin')->group(function () {
  Route::resource('productos', 'ProductoController');
  //ruta para editar, trae la vista edit con el id seleccionado
  Route::GET('productos/{{sku}}/edit', 'ProductoController@edit',function ($sku){ 
    return 'productos '.$sku;
  });
  //ruta para crear nuevo producto, trae la vista create
  Route::GET('productos/create ', 'ProductoController@create');
  //ruta para update, eejecuta la accion update
  Route::PUT('productos/{{sku}}', 'ProductoController@update',function ($sku){ 
    return 'productos '.$sku;
  });
});
//admin/ventas
Route::prefix('admin')->group(function () {
  Route::resource('ventas', 'VentaController');
  //ruta para editar, trae la vista edit con el id seleccionado

});


//admin/categorias

Route::prefix('admin')->group(function () {
  Route::resource('categorias', 'CategoriaController');
//ruta para editar, trae la vista edit con el id seleccionado
  Route::GET('categorias/{{id}}/edit', 'CategoriaController@edit',function ($id){ 
    return 'categorias '.$id;
  });
    //ruta para update, eejecuta la accion update
  Route::PUT('categorias/{{id}}', 'CategoriaController@update',function ($id){ 
    return 'categorias '.$id;
  });
});



//admin/usuarios
Route::prefix('admin')->group(function () {
  Route::resource('usuarios', 'UsuarioController');
  Route::get('usuarios/create','UsuarioController@create');
  Route::GET('usuarios/{{id}}/edit', 'UsuarioController@edit',function ($id){ 
    return 'users'.$id;
  });
  Route::PUT('usuarios/{{id}}', 'UsuarioController@update',function ($id){ 
    return 'users'.$id;
  });

});


//login y register
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

//ruta home del cliente
Route::get('/client', 'ClientController@index');

//client/catalogo
Route::prefix('client')->group(function () {
  Route::resource('catalogo', 'CatalogoController');

});

//client/carrito
Route::prefix('client')->group(function () {
  Route::resource('carrito', 'CarritoController');
  Route::PUT('carrito/{{sku}}', 'CarritoController@update',function ($sku){ 
    return 'productos'.$sku;
  });
});

//client/comprar
Route::prefix('client')->group(function () {
  Route::resource('comprar', 'CompraController');
});

//client/pedidos
Route::prefix('client')->group(function () {
  Route::resource('pedidos', 'PedidoController');
  Route::get('pedidos/{{id_pedido}}','PedidoController@show',function ($id_pedido){ 
    return 'facturas'.$id_pedido;
  });
});

